let num1 = Number(prompt(`Enter a Number: `));
console.log(`The number you provided is ${num1}`);


for (let num = num1; num > 50; num--){
	
	if (num % 10 === 0){
		console.log(`The number is divisible by 10. Skipping the number`);
		continue;
	}
	
	if (num % 5 === 0){
		console.log(num)
	}

	if (num <= 0){
		break;
	}
} 

console.log(`The current value is at 50 . Terminating the loop.`)

